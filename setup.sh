#!/bin/bash
# OPNFV Setup

sudo apt update
sudo apt upgrade -y
sudo apt install -y software-properties-common
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt update
sudo apt install -y ansible

ansible-galaxy install -r requirements.yml

ansible-playbook --ask-vault-pass site.yml
